# wellfed

`wellfed` is a feed aggregator. 

Instead of configuring your RSS reader with all the feeds you want to read, you
can setup `wellfed` once on a server and then point your reader to this one
feed.

Then, if you switch RSS readers or want a reader on your phone and your
desktop, the setup is a breeze.


## Install

 1. Find a place to host static web pages.
 1. Checkout this repo
 1. Run `npm install` to pull in the readability node dependencies.
 1. Copy `wellfeed.yaml.sample` to `wellfed.yaml` and edit to your liking. You
    can add RSS feeds and also configure the path to the generated output.xml
    file. Ensure the path is in your web directory.
 1. Create a virtual environment (optional)
 1. pip install -r requirements.txt
 1. Start with `./wellfed.py` - you may see lots of output about xml parsing errors. 
    You can ignore those.
 1. When the command completes, point your RSS reader to the output.xml file you configured in
    wellfed.yaml. Or, point your browser to the index.html file.
 1. Create a cron job to run as frequently as you need.

## Features

In addition to the normal RSS aggregator options, wellfed has the following
special features:

 * `wellfed` fetches the full content and converts it to a simplified reader
   view using [the same code Firefox uses to produce the in browser reader
   view](https://github.com/mozilla/readability). So you don't have to click
   through the RSS entry to get the content.

 * You can make certain feeds sticky for a given number of hours. For example,
   if you have several feeds that produce 10 articles a day, you may want to
   let those scroll off your feed so you always get fresh content. On the other
   hand, if you follow a few RSS feeds that only posts a few times a year, you
   can make them sticky, for say 48 hours, and that ensures they will stay at
   the top of your feed for the number of hours you designate after their
   published date.

## Bugs

`wellfed.py` tries to catch all the things that can go wrong when parsing
random content from the Internet, but there is surely more that I have not yet
experienced. Feel free to open an issue if you encounter one.

## Attribution

The fork and knife logo was uploaded to
[wikimedia commons](https://commons.wikimedia.org/wiki/File:Aiga_restaurant_knife-fork_crossed.png)
by the ["Copyleft" user](https://commons.wikimedia.org/wiki/User:Copyleft) and
is released under the [Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/3.0/deed.en).

