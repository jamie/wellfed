// See: https://github.com/mozilla/readability
var { Readability } = require('@mozilla/readability');
var JSDOM = require('jsdom').JSDOM;
const fetch = require('node-fetch');

let url = process.argv[2];

function output_readerview(url, text) {
  var doc = new JSDOM(text, {
    url: url 
  });
  let reader = new Readability(doc.window.document);
  let article = reader.parse();
  console.log(article.content)
}
fetch(url)
  .then(res => res.text())
  .then(text => output_readerview(url, text))



