#!/usr/bin/env python

import feedparser
from datetime import datetime
from feedgen.feed import FeedGenerator
import pytz
import subprocess
import time
from io import StringIO
from lxml import etree
import os
import yaml
import sys
import re
import signal
from http import client
from jinja2 import Environment, FileSystemLoader, select_autoescape
import sqlite3
import json
from urllib.parse import urlparse
import shutil
from bs4 import BeautifulSoup

WELLFED_LOG_LEVEL_DEBUG = 0 
WELLFED_LOG_LEVEL_INFO = 1 
WELLFED_LOG_LEVEL_ERROR = 2 
WELLFED_LOG_LEVEL_QUIET = 3

WELLFED_LOG_PRIORITY_DEBUG = 1
WELLFED_LOG_PRIORITY_INFO = 2
WELLFED_LOG_PRIORITY_ERROR = 3

if int(os.environ.get("WELLFED_LOG_LEVEL", WELLFED_LOG_LEVEL_ERROR)) == WELLFED_LOG_LEVEL_DEBUG:
    try:
        import psutil
    except ModuleNotFoundError as e:
        print("Cannot find psutil, not printing memory stats.")

def main():
    wellfed = Wellfed()
    # Adjust loglevel via environment variable, e.g.:
    # WELLFED_LOG_LEVEL=0 ./wellfed.py for debug.
    log_level = os.environ.get('WELLFED_LOG_LEVEL', WELLFED_LOG_LEVEL_ERROR)
    if log_level:
        wellfed.log_level = int(log_level)
        
    wellfed.main()

class Wellfed: 
    conf = None 
    log_level = WELLFED_LOG_LEVEL_ERROR

    # Keep track of etags keyed to the feed link so we can politely request
    # updates and not overload the server when there is nothing new.

    etags = {}

    # Maintain a simple list of links that are already cached so we don't have
    # to re-save them.
    cached_links = [] 

    # Known, common extensions that we won't try to parse as HTML
    # in order to get a readerview version of their content.
    skip_parse_extensions = [
        ".pdf",
        ".jpeg",
        ".jpg",
        ".png",
        ".gif",
        ".docx",
        ".xlsx",
        ".odt",
        ".ods",
        ".webm",
        ".mov",
        ".mp3",
        ".exe"
    ]

    env = None
    template = None

    non_unicode_regexp = re.compile(u'[^\u0020-\uD7FF\u0009\u000A\u000D\uE000-\uFFFD\U00010000-\U0010FFFF]+')
    html_tags_regexp = re.compile('<.*?>')
    img_regexp = re.compile('<img.* src="(.*?)"', re.IGNORECASE)

    # sqlite database variables.
    con = None
    cur = None

    def log(self, msg, priority = WELLFED_LOG_PRIORITY_INFO):
        if priority > self.log_level:
            timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print("{0}: {1} {2}".format(timestamp, priority, msg), flush=True)

    def initialize_db(self):
        exists = os.path.exists(self.conf["db"])
        self.con = sqlite3.connect(self.conf["db"])
        self.cur = self.con.cursor()
        if not exists:
            # Create the necessary tables.
            self.cur.execute("CREATE TABLE etag (link PRIMARY KEY, etag)")
            self.cur.execute("CREATE TABLE cache (link PRIMARY KEY, sort int, sticky, timestamp int, source, readerview, post)")
            self.con.commit()
        else:
            # Populate some variables into memory so we reduce the number of sql queries on each run.
            for row in self.cur.execute("SELECT link, etag FROM etag"):
                link = row[0]
                etag = row[1]
                self.etags[link] = etag
            for row in self.cur.execute("SELECT link FROM cache"):
                self.cached_links.append(row[0])

    # XML can only print unicode or ASCII. This function strips out any
    # characters that will cause it to fail. See:
    # https://stackoverflow.com/questions/8733233/filtering-out-certain-bytes-in-python
    def strip_non_unicode(self, text):
        return re.sub(self.non_unicode_regexp, '', text)

    def strip_html_tags(self, text):
        return re.sub(self.html_tags_regexp, '', text)

    def set_conf(self):
        self.log("Loading configuration file.")
        # Default locations of configuration file is in the same directory as
        # the program.
        conf_paths = [ "wellfed.yaml" ]

        # Optionally pass path as first argument.
        if 1 in sys.argv:
            conf_paths.insert(sys.argv[1])

        # Find the file.
        for conf_path in conf_paths:
            if os.path.isfile(conf_path):
                self.log("Found: {0} ".format(conf_path))
                with open(conf_path, 'r') as file:
                    conf_data = file.read(10000)
                self.conf = yaml.safe_load(conf_data)
                break
        if not self.conf:
            raise RuntimeError("Failed to find a configuration file.")

        self.env = Environment(
            loader=FileSystemLoader('templates'),
            autoescape=select_autoescape(['html', 'xml'])
        )

        self.template = self.env.get_template("index.html.j2")

    def signal_handler(self, sig, frame):
        if sig == signal.SIGINT:
            self.log("ctl-c pressed.")
            sys.exit(0)

    # Given the defined feeds, populate the cached table with the latest
    # posts.
    def populate_cached_items(self):
        for feed in self.conf["feeds"]:
            url = feed["url"]
            source = feed["source"]
            # etags keep track of whehter a feed has fresh content or not.
            # Our first time through, there will not be an etag.
            if url not in self.etags:
                self.etags[url] = None
            try:
                f = feedparser.parse(url, etag=self.etags[url])
            except OSError as e:
                self.log("OS Error while fetching URL: {0}".format(url), WELLFED_LOG_PRIORITY_ERROR)
                continue
            except client.IncompleteRead as e:
                self.log("Incomplete read error while fetching URL: {0}".format(url), WELLFED_LOG_PRIORITY_ERROR)
                continue

            # Update the etag so our next round will be more efficient.
            if "etag" in f and f.etag != self.etags[url]:
                self.log("Updating etag to {0} for {1}.".format(f.etag, url))
                self.etags[url] = f.etag

            for p in f.entries:
                link = p.link
                # If we have already collected the link, we want to move on.
                if link in self.cached_links:
                    # We already have this one so move on.
                    continue

                self.log(f"Entry found, link: {link}, title: {p.title}", WELLFED_LOG_PRIORITY_DEBUG)
                published = None
                if "published_parsed" in p:
                    published = p.published_parsed
                elif "updated_parsed" in p:
                    published = p.updated_parsed
                elif "created_parsed" in p:
                    published = p.created_parsed

                if not published:
                    # Skip it
                    message = f"Skipping {p.title} ({link}). Can't find published date."
                    self.log(message.format(link), WELLFED_LOG_PRIORITY_ERROR)
                    continue

                # By default sort time is the published timestamp.
                timestamp = sort = time.mktime(published)

                sticky = False
                if "sticky" in feed:
                    # If this entry should be sticky, we adjust the sort,
                    # setting it sticky hours ahead of it's date so it will
                    # stay at the top of the feed for the sticy period. We
                    # also flag it as sticky so it can be themed differently.
                    sort = timestamp + (feed["sticky"] * 60 * 60)
                    sticky = True

                link = p.link
                self.log("Adding link {0} to cached_items and links".format(link), WELLFED_LOG_PRIORITY_DEBUG)

                # Add to the database cache.
                cache_query = "REPLACE INTO cache (link, sort, sticky, timestamp, source, readerview, post) VALUES(?, ?, ?, ?, ?, ?, ?)"
                self.cur.execute(cache_query, (link, sort, sticky, timestamp, source, None, json.dumps(p)))

        for url in self.etags:
            self.cur.execute("REPLACE INTO etag VALUES(?, ?)", (url, self.etags[url]))

        self.con.commit()

    """
    We maintain both a timestamp and a sort field in the cache table. The timestamp is the
    actual timestamp, the sort field is our doctored sort. To avoid having one feed dominate
    our list, we maintain limits on how many items per feed are allowed. The sort field
    is our enforced sort order based on our criteria.
    """
    def resort_links(self):
        # Get all the entries from the database sorted by our doctored sort field.
        # with the value set to the source.
        links = {}
        for row in self.cur.execute("SELECT link, source FROM cache ORDER BY sort DESC"):
            link = row[0]
            source = row[1]
            links[link] = source

        # Now we have them in chronlogical order but... we have to re-order
        # one more time to ensure the top 25 respects the max posts per
        # feed rule. After 25, we can let high volume feeds dominant, but
        # not before.
        sorted_links = []

        # Keep track of how many articles have come from each feed so we
        # can enforce the max_posts_per_feed rule.
        count_per_feed = {}

        # The max_posts_per_feed only applied to the first 25. Keep track
        # of posts that were shunted so they can be re-incorporated into
        # the feed after we have listed the first 25.
        shunted_links = []

        # Keep track of all the links we've seen so we can garbage collect.
        seen_links = []
        count = -1 
        for link in links:
            count = count + 1
            if link in seen_links:
                # Skip dupes
                continue
            seen_links.append(link)

            if count > self.conf["max_posts_generated"]:
                # We've hit the max we are going to publish. Everything else is too old.
                break 

            # For the first 25 we enforce a maximum number of posts
            # per feed to avoid having a single feed saturate the output.
            if count < 25:
                count_key = links[link]
                if not count_key in count_per_feed:
                    count_per_feed[count_key] = 1
                else:
                    if count_per_feed[count_key] > self.conf["max_posts_per_feed"]:
                        shunted_links.append(link)
                        continue
                    else:
                        count_per_feed[count_key] = count_per_feed[count_key] + 1
            if count == 25:
                # Take a break from the normal flow to bring in the previously
                # shunted items.
                for shunted_link in shunted_links:
                    sorted_links.append(shunted_link)
                    count = count + 1

            sorted_links.append(link)

        # Garbage collection. Purge the database of cached items no longer in use.
        delete_me = []
        for link in links:
            if link not in seen_links:
                # If this link wasn't seen on this round, it has surely been cycled out.
                self.log("Removing link {0} from cached_items.".format(link), WELLFED_LOG_PRIORITY_DEBUG)
                self.cur.execute("DELETE FROM cache WHERE link = ?", (link,))
                delete_me.append(link)

        return sorted_links

    def get_readerview(self, link):
        # Most feeds don't provide the full content. Boo! We want it
        # all. We run an external program to get the "reader view" of
        # the content - just the text please.  And we cache the results
        # to be more efficient.
        readerview = None

        # These should probably be refactored into the conf file.
        readerview_cmd = [ '/usr/bin/node', 'readerview.js' ]

        # We need to convert the HTML returned by the link to a
        # simplified, readerview.  But, we can't be sure the link
        # returns text. It could be a PDF, image file etc. As a
        # first pass, we skip links ending in common binary
        # formats.
        ext = os.path.splitext(link)[1]
        if ext and ext in self.skip_parse_extensions:
            readerview = "No preview for link for known binary extension ({0}), Link: {1}."
            readerview = readerview.format(ext, link)
            self.log(readerview)
        else:
            cmd = readerview_cmd.copy()
            cmd.append(link)
            try:
                result = subprocess.run(cmd, capture_output=True, text=True, timeout=30)
                readerview = result.stdout
            except subprocess.TimeoutExpired as e:
                readerview = "Timed out getting readerview for {0}".format(link)
                self.log(readerview, WELLFED_LOG_PRIORITY_ERROR)
                self.log(e, WELLFED_LOG_PRIORITY_DEBUG)

            # Test for valid xml. This is just a test so that we
            # don't try to stuff non xml data into the atom feed,
            # which will cause the atom feed creation to fail. You
            # might think using lxml.html.fromstring() would be
            # less noisy, but sadly, it also doesn't seem to throw
            # any exceptions even when you pass it data from a png
            # file. So better to have noisy xml parsing that warns
            # us about things we don't care about then no parsing
            # at all.
            try:
                etree.fromstring(readerview)
            except etree.XMLSyntaxError as e:
                # We get a lot of XMLSyntaxError exceptions, but
                # most of them won't cause the atom feed generation
                # to fail. This one, however, seems to indicate we
                # don't have anything any where near a real XML
                # document (e.g. it's a binary format, like a link
                # to an image). So, let's catch this error and
                # reset the readerview so we don't cause a crash
                # later.
                if str(e).startswith("PCDATA invalid Char"):
                    readerview = """
                        This link caused an XML parsing exception.
                        If this link has an extension('{0}'), maybe
                        we should exclude it. Here's the link: {1}.
                    """
                    readerview = readerview.format(ext, link)
                    self.log(readerview, WELLFED_LOG_PRIORITY_ERROR)
                else:
                    # Any other error we will let on through - probably
                    # ok and if it's fatal we'll see it in the log and
                    # can add a proper exception.
                    message = "Exception: {0} for link: {1} and extension: {2}."
                    self.log(message.format(type(e).__name__, link, ext))
                    self.log(e)

        readerview = self.strip_non_unicode(readerview)
        # Use Beautiful soup to try to fix any lingering html problems
        # that might cause errors.
        bs = BeautifulSoup(readerview, "html.parser")
        return bs.prettify() 

    def main(self):
        # Register our signal handler.
        signal.signal(signal.SIGINT, self.signal_handler)
        self.set_conf()
        # Report memory so we can detect memory leaks
        if self.log_level == WELLFED_LOG_LEVEL_DEBUG:
            try:
                mem = psutil.Process(os.getpid()).memory_info().rss / 1024 / 1024
                self.log("Memory usage: {0} MB".format(mem), WELLFED_LOG_PRIORITY_DEBUG)
            except NameError:
                pass
        self.initialize_db()
        tz = pytz.timezone(self.conf["tz"])
        self.populate_cached_items()

        feed = FeedGenerator()
        feed.id(self.conf["url"])
        feed.title(self.conf["title"])
        feed.link(href=self.conf["link"])
        feed.author(self.conf["author"])

        html = {
            "title": self.conf["title"],
            "description": self.conf["title"],
            "entries": []
        }
        # Run one query to pull in all of our cached items.
        cached_items = {}
        for row in self.cur.execute("SELECT link, sticky, timestamp, source, readerview, post FROM cache ORDER BY sort DESC"):
            link = row[0]
            cached_items[link] = {
                'sticky': row[1],
                'timestamp': row[2],
                'source': row[3],
                'readerview': row[4],
                'post': json.loads(row[5])
            }

        sorted_links = self.resort_links()
        for link in sorted_links:
            item = cached_items[link]
            post = cached_items[link]["post"]
            # We only pull in the readerview at the last minute to avoid wasited
            # readerviews on posts that don't make the cut.
            if item["readerview"]:
                readerview = item["readerview"]
            else:
                readerview = self.get_readerview(link)
                if readerview:
                    self.cur.execute("UPDATE cache SET readerview = ? WHERE link = ?", (readerview, link))
            published = datetime.fromtimestamp(item["timestamp"], tz=tz)
            title = self.strip_non_unicode(post["title"])

            # The "description" is usually the first few paragraphs which
            # is typically more then what is given as the "summary", so we
            # use the description as the summary.
            try:
                summary = self.strip_non_unicode(post["summary"])
                # Some summaries have someting like "end of sentence</p><p>Begining of sentence.
                # If we strip the tags, we don't have a space. So let's replace those with spaces
                # first.
                summary = re.sub("</(p|P|br|BR)><(p|P|br|BR)>", " ", summary)
                summary = self.strip_html_tags(summary)[0:300] + "..."
            except KeyError:
                # If the post has no description, don't crash.
                summary = "No description"

            # Check for required atom values.
            if not link:
                self.log("Entry missing a link, title is {0}.".format(title))
                continue

            if not title:
                self.log("Entry missing a title, link is {0}.".format(link))
                continue

            if not item["timestamp"]:
                self.log("Entry missing a timestamp, link is {0}".format(link))

            # By default entries are prepended to the beginning as we go.
            # We want to append because we want the newest at the top.
            entry = feed.add_entry(order="append")
            post_id = link
            entry.id(post_id)
            entry.author({ "name": item["source"] })
            entry.title(title)
            entry.link(href=link)
            entry.summary(summary)
            entry.description(readerview)
            entry.published(published)
            entry.updated(datetime.fromtimestamp(item["timestamp"], tz=tz))

            # Fish out a lead image for the summary page.                
            lead_image_src = None
            image_match = re.search(self.img_regexp, readerview)
            if image_match:
                lead_image_src = image_match.group(1)

            # We include sticky so we can style the sticky ones
            # differently.
            sticky_class = ""
            if item["sticky"]:
                sticky_class = "sticky" 

            html_entry = {
               "title": title,
               "link": link,
               "domain": urlparse(link).netloc,
               "summary": summary,
               "body": readerview,
               "source": item["source"],
               "date": published.strftime('%Y-%m-%d %H:%M:%S'),
               "lead_image_src": lead_image_src,
               "sticky_class": sticky_class
            }
            html["entries"].append(html_entry)

        self.con.commit()

        # We sometimes crash out here if there is something unexpected in
        # the feed. We could use try/catch to avoid ever crashing, but it
        # would be nice to solve those problems above so we don't catch any
        # exceptions. Please report these as bugs so we can fix them.
        feed.atom_file(self.conf["feed_target"])

        with open(self.conf["html_target"], "w") as f:
            f.write(self.template.render(html=html))

        # Now ensure all of our static assets are copied
        html_base = os.path.dirname(self.conf["html_target"])
        for file in os.listdir("static"):
            src = "static/{0}".format(file)
            dest = "{0}/{1}".format(html_base, file)
            if (not os.path.exists(dest)) or (os.stat(src).st_mtime - os.stat(dest).st_mtime > 1):
                shutil.copyfile(src, dest)

if __name__ == "__main__":
	sys.exit(main())
